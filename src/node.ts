export type Node<T> = T & {
  nodes?: Node<T>[]
}

export type Indexed = {
  idx: number
}

export function indexNodes<T>(root: Node<T>): Node<T & Indexed> {
  let idx = -1
  function recurse(node: Node<T>): Node<T & Indexed> {
    idx += 1
    return {
      idx,
      ...node,
      nodes: node.nodes?.map((child) => recurse(child)),
    }
  }
  return recurse(root)
}
