import * as d3 from "d3"
import * as contentBox from "./contentBox"
import * as node from "./node"

const DEFAULT_TRANSITION_DURATION = 750

export function treeHierarchy<T>({
  root,
  width,
  height,
}: {
  root: node.Node<T>
  width: number
  height: number
}): d3.HierarchyPointNode<node.Node<T>> {
  return d3.tree<node.Node<T>>().size([width, height])(
    d3.hierarchy(root, (curr) => curr.nodes),
  )
}

export type TreeContainer = d3.Selection<
  HTMLDivElement,
  undefined,
  null,
  undefined
>

export function createTreeContainer(cb: contentBox.ContentBox): TreeContainer {
  const { width, height, topPadding, leftPadding } = cb
  const sel = d3.create("div").style("position", "relative")
  sel
    .append("svg")
    .classed("tree", true)
    .attr("width", width)
    .attr("height", height)
    .append("g")
    .classed("content", true)
    .attr("transform", `translate(${leftPadding}, ${topPadding})`)
  return sel
}

export type TreeDatum = {
  name: string
  classes?: string[]
  description?: string
}

export type HTreeJoinArgs<T> = {
  treeContainer: TreeContainer
  rootHierarchy: d3.HierarchyPointNode<node.Node<T>>
  transitionDuration?: number
  nodeClickHandler?: (e: Event, d: d3.HierarchyPointNode<node.Node<T>>) => void
}

function linkKey<T extends TreeDatum>(
  d: d3.HierarchyPointLink<node.Node<T>>,
): string {
  return `${d.source.data.name}--${d.target.data.name}`
}

function nodeKey<T extends TreeDatum>(
  d: d3.HierarchyPointNode<node.Node<T>>,
): string {
  return `${d.data.name}`
}

function joinPaths<T extends TreeDatum>({
  treeContainer,
  rootHierarchy,
  transitionDuration = DEFAULT_TRANSITION_DURATION,
}: HTreeJoinArgs<T>): void {
  const links = rootHierarchy.links()
  const linker = d3
    .linkHorizontal<
      d3.HierarchyPointLink<node.Node<T>>,
      d3.HierarchyPointNode<node.Node<T>>
    >()
    .x((d) => d.y)
    .y((d) => d.x)
  treeContainer
    .select("g.content")
    .selectAll<SVGPathElement, d3.HierarchyPointLink<node.Node<T>>>("path.link")
    .data(links, linkKey)
    .join(
      (enter) =>
        enter
          .append("path")
          .classed("link", true)
          .attr(
            "d",
            (d) => linker(d)?.replace(/M([^C]+).*/, "M $1 C $1 $1 $1") || "",
          )
          .style("display", "none")
          .transition()
          .delay((d) => (d.source.depth + 1) * transitionDuration)
          .style("display", "block")
          .transition()
          .duration(transitionDuration)
          .attr("d", linker),
      (update) =>
        update.transition().duration(transitionDuration).attr("d", linker),
    )
}

export function hTreeJoin<T extends TreeDatum & node.Indexed>({
  treeContainer,
  rootHierarchy,
  transitionDuration = DEFAULT_TRANSITION_DURATION,
  nodeClickHandler,
}: HTreeJoinArgs<T>): void {
  const data = rootHierarchy.descendants()
  const g = treeContainer.select<SVGGElement>("g.content")

  joinPaths({ treeContainer, rootHierarchy, transitionDuration })
  g.selectAll<SVGGElement, d3.HierarchyPointNode<node.Node<T>>>("g.node")
    .data(data, nodeKey)
    .join(
      (enter) =>
        enter
          .append("g")
          .classed("node", true)
          .append("g")
          .classed("control", true)
          .call((sel) =>
            nodeClickHandler ? sel.on("click", nodeClickHandler) : sel,
          )
          .append("circle")
          .classed("node", true)
          .classed("node2", true)
          .attr("cx", (d) => (d.parent ? d.parent.y : d.y))
          .attr("cy", (d) => (d.parent ? d.parent.x : d.x))
          .attr("r", 16)
          .style("transform-origin", (d) => `${d.y}px ${d.x}px`)
          .call((sel) =>
            sel
              .style("display", "none")
              .transition()
              .delay((d) => d.depth * transitionDuration)
              .style("display", "block")
              .transition()
              .duration(transitionDuration)
              .attr("cx", (d) => d.y)
              .attr("cy", (d) => d.x),
          )
          .select(function getParent() {
            const res = this.parentNode
            if (!(res instanceof SVGGElement))
              throw Error("Parent should be an SVGGElement")
            return res
          })
          .append("circle")
          .classed("node", true)
          .classed("node1", true)
          .attr("cx", (d) => (d.parent ? d.parent.y : d.y))
          .attr("cy", (d) => (d.parent ? d.parent.x : d.x))
          .attr("r", 10)
          .style("transform-origin", (d) => `${d.y}px ${d.x}px`)
          .call((sel) =>
            sel
              .style("display", "none")
              .transition()
              .delay((d) => d.depth * transitionDuration)
              .style("display", "block")
              .transition()
              .duration(transitionDuration)
              .attr("cx", (d) => d.y)
              .attr("cy", (d) => d.x),
          )
          .select(function getParentParent() {
            const res = this.parentNode?.parentNode
            if (!(res instanceof SVGGElement))
              throw Error("Parent should be an SVGGElement")
            return res
          })
          .append("text")
          .classed("node", true)
          .attr("x", (d) => (d.parent ? d.parent.y : d.y))
          .attr("y", (d) => (d.parent ? d.parent.x : d.x + 35))
          .attr("text-anchor", "middle")
          .call((sel) =>
            sel
              .style("display", "none")
              .transition()
              .delay((d) => d.depth * transitionDuration)
              .style("display", "block")
              .transition()
              .duration(transitionDuration)
              .attr("x", (d) => d.y)
              .attr("y", (d) => d.x + 35),
          )
          .text((d) => d.data.name),
      (update) =>
        update.select(function repositionChildren(d) {
          const circles = this.querySelectorAll("circle")
          const texts = this.querySelectorAll("text")
          d3.selectAll(circles)
            .transition()
            .duration(transitionDuration)
            .attr("cx", d.y)
            .attr("cy", d.x)
            .style("transform-origin", `${d.y}px ${d.x}px`)
          d3.selectAll(texts)
            .transition()
            .duration(transitionDuration)
            .attr("x", d.y)
            .attr("y", d.x + 35)
          return this
        }),
    )
  treeContainer
    .selectAll<HTMLDivElement, d3.HierarchyPointNode<node.Node<T>>>("div.node")
    .data(data, nodeKey)
    .join(
      (enter) =>
        enter
          .append("div")
          .classed("node", true)
          .style("position", "absolute")
          .style("top", (d) => `${d.x + 40}px`)
          .style("left", (d) => `${d.y}px`)
          .text(
            (d) =>
              d.data.description ||
              `This is the description of node ${d.data.name}`,
          ),
      (update) =>
        update
          .transition()
          .duration(transitionDuration)
          .style("top", (d) => `${d.x + 40}px`)
          .style("left", (d) => `${d.y}px`),
    )
}
