import { describe, test, expect } from "vitest"
import * as itertools from "./itertools"

describe("count", () => {
  test("count from 0 to 9", () => {
    const counter = itertools.count()
    const xs = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    const ys = xs.map((_) => counter.next().value)
    expect(xs).toEqual(ys)
  })
  test("count from 1 to 9", () => {
    const counter = itertools.count(1)
    const xs = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    const ys = xs.map((_) => counter.next().value)
    expect(xs).toEqual(ys)
  })
})

describe("range", () => {
  test("range(10) goes from 0 to 9 inclusive", () => {
    const xs = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    const rg = itertools.range(10)
    expect(xs).toEqual([...rg])
  })
  test("range(1, 10) goes from 1 to 9 inclusive", () => {
    const xs = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    const rg = itertools.range(1, 10)
    expect(xs).toEqual([...rg])
  })
  test("range(2, 10, 2) is even numbers from 2 to 8 inclusive", () => {
    const xs = [2, 4, 6, 8]
    const rg = itertools.range(2, 10, 2)
    expect(xs).toEqual([...rg])
  })
  test("range with for of loop", () => {
    const xs: number[] = []
    // eslint-disable-next-line no-restricted-syntax
    for (const i of itertools.range(10)) {
      xs.push(i)
    }
    expect(xs).toEqual([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
  })
})
