export type ContentBox = {
  width: number
  height: number
  topPadding: number
  rightPadding: number
  bottomPadding: number
  leftPadding: number
  contentWidth: number
  contentHeight: number
}

export type ContentBoxDescription = {
  width: number
  height: number
  topPadding?: number
  rightPadding?: number
  bottomPadding?: number
  leftPadding?: number
}

function assertContentWidth(contentWidth: number, width: number): string[] {
  if (contentWidth < 0) {
    return [
      "Invalid `leftPadding` and `rightPadding`." +
        " The resulting `contentWidth` would be negative.",
    ]
  }
  if (contentWidth > width) {
    return [
      "Invalid `leftPadding` and `rightPadding`." +
        " The resulting `contentWidth` would be greater than `width`.",
    ]
  }
  return []
}

function assertContentdHeight(
  contentdHeight: number,
  height: number,
): string[] {
  if (contentdHeight < 0) {
    return [
      "Invalid `topPadding` and `bottomPadding`." +
        " The resulting `contentdHeight` would be negative.",
    ]
  }
  if (contentdHeight > height) {
    return [
      "Invalid `topPadding` and `bottomPadding`." +
        " The resulting `contentdHeight` would be greater than `height`.",
    ]
  }
  return []
}

function throwRangeErrors(...errors: string[][]): void {
  const flatErrors = errors.flatMap((x) => x)
  if (flatErrors.length) {
    throw new RangeError(["Invalid values:", ...flatErrors].join("\n\t"))
  }
}

export function contentBox(
  {
    width = 0,
    height = 0,
    topPadding = 0,
    rightPadding = 0,
    bottomPadding = 0,
    leftPadding = 0,
  }: ContentBoxDescription = { width: 0, height: 0 },
): ContentBox {
  const contentWidth = width - leftPadding - rightPadding
  const contentHeight = height - topPadding - bottomPadding
  throwRangeErrors(
    assertContentWidth(contentWidth, width),
    assertContentdHeight(contentHeight, height),
  )
  return {
    width,
    height,
    topPadding,
    rightPadding,
    bottomPadding,
    leftPadding,
    contentWidth,
    contentHeight,
  }
}
