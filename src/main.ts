import * as d3 from "d3"
import "./main.css"
import * as contentBox from "./contentBox"
import * as itertools from "./itertools"
import * as node from "./node"
import * as tree from "./tree"

const [WIDTH, HEIGHT] = [1920, 1080].map((x) => x / 3)
const TOP_PADDING = 20
const RIGHT_PADDING = 100
const BOTTOM_PADDING = 20
const LEFT_PADDING = 40

function createTree(cb: contentBox.ContentBox) {
  const div = tree.createTreeContainer(cb)
  const divNode = div.node()
  if (!divNode) throw Error("Failed to create tree")
  document.getElementById("app")?.appendChild(divNode)
  return div
}

const treeContentBox = contentBox.contentBox({
  width: WIDTH,
  height: HEIGHT,
  topPadding: TOP_PADDING,
  rightPadding: RIGHT_PADDING,
  bottomPadding: BOTTOM_PADDING,
  leftPadding: LEFT_PADDING,
})
const treeContainer = createTree(treeContentBox)

let root = {
  name: "root",
  classes: ["root", "inProgress"],
  nodes: [
    { name: "foo" },
    { name: "bar", nodes: [{ name: "baz" }, { name: "quo" }] },
  ],
  description: "This is the root node.",
}

function nodeClickHandler(
  _e: Event,
  d: d3.HierarchyPointNode<node.Node<tree.TreeDatum & node.Indexed>>,
) {
  const { idx } = d.data
  d3.selectAll<
    HTMLDivElement,
    d3.HierarchyPointNode<node.Node<tree.TreeDatum & node.Indexed>>
  >("div.node").each(function updateActive(currD) {
    const currIdx = currD.data.idx
    if (currIdx !== idx) this.classList.remove("active")
    else this.classList.toggle("active")
  })
}

const rootHierarchy = tree.treeHierarchy({
  root: node.indexNodes(root),
  width: treeContentBox.contentHeight,
  height: treeContentBox.contentWidth,
})
tree.hTreeJoin({ treeContainer, rootHierarchy, nodeClickHandler })

const newId = itertools.count()
const addNode = document.getElementById("addNode")
addNode?.addEventListener("click", function handleAddNode() {
  root = {
    ...root,
    nodes: [...root.nodes, { name: `new-${newId.next().value}` }],
  }
  tree.hTreeJoin({
    treeContainer,
    rootHierarchy: tree.treeHierarchy({
      root: node.indexNodes(root),
      width: treeContentBox.contentHeight,
      height: treeContentBox.contentWidth,
    }),
    nodeClickHandler,
  })
})
