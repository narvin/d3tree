import { describe, test, expect } from "vitest"
import * as node from "./node"

describe("indexNodes", () => {
  test("no children", () => {
    expect(node.indexNodes({ name: "a" })).toStrictEqual({
      name: "a",
      idx: 0,
      nodes: undefined,
    })
  })
  test("2 levels", () => {
    expect(
      node.indexNodes({ name: "a", nodes: [{ name: "b" }, { name: "c" }] }),
    ).toStrictEqual({
      idx: 0,
      name: "a",
      nodes: [
        { idx: 1, name: "b", nodes: undefined },
        { idx: 2, name: "c", nodes: undefined },
      ],
    })
  })
  test("3 levels", () => {
    expect(
      node.indexNodes({
        name: "a",
        nodes: [
          { name: "b", nodes: [{ name: "d", nodes: undefined }] },
          { name: "c", nodes: [{ name: "e", nodes: undefined }] },
        ],
      }),
    ).toStrictEqual({
      idx: 0,
      name: "a",
      nodes: [
        { idx: 1, name: "b", nodes: [{ idx: 2, name: "d", nodes: undefined }] },
        { idx: 3, name: "c", nodes: [{ idx: 4, name: "e", nodes: undefined }] },
      ],
    })
  })
})
