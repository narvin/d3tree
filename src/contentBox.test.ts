import { describe, expect, test } from "vitest"
import * as contentBox from "./contentBox"

describe("contentBox", () => {
  test("no args", () => {
    expect(contentBox.contentBox()).toEqual({
      width: 0,
      height: 0,
      topPadding: 0,
      rightPadding: 0,
      bottomPadding: 0,
      leftPadding: 0,
      contentWidth: 0,
      contentHeight: 0,
    })
  })
  test("all args", () => {
    expect(
      contentBox.contentBox({
        width: 400,
        height: 300,
        topPadding: 1,
        rightPadding: 2,
        bottomPadding: 3,
        leftPadding: 4,
      }),
    ).toEqual({
      width: 400,
      height: 300,
      topPadding: 1,
      rightPadding: 2,
      bottomPadding: 3,
      leftPadding: 4,
      contentWidth: 394,
      contentHeight: 296,
    })
  })
  test("some args", () => {
    expect(
      contentBox.contentBox({
        width: 400,
        height: 300,
        topPadding: 5,
        leftPadding: 10,
      }),
    ).toEqual({
      width: 400,
      height: 300,
      topPadding: 5,
      rightPadding: 0,
      bottomPadding: 0,
      leftPadding: 10,
      contentWidth: 390,
      contentHeight: 295,
    })
  })
  test("invalid side padding", () => {
    expect(() =>
      contentBox.contentBox({
        width: 400,
        height: 300,
        leftPadding: 250,
        rightPadding: 250,
      }),
    ).toThrow(RangeError)
  })
})
