export function* count(start: number = 0): Generator<number, void, unknown> {
  let next = start
  while (true) {
    yield next
    next += 1
  }
}

export function range(stop: number): Generator<number, void, unknown>
export function range(
  start: number,
  stop: number,
  step?: number,
): Generator<number, void, unknown>
export function* range(
  start: number = 0,
  stop: number = Infinity,
  step: number = 1,
): Generator<number, void, unknown> {
  const realStart = arguments.length === 1 ? 0 : start
  const realStop = arguments.length === 1 ? start : stop
  for (let i = realStart; i < realStop; i += step) {
    yield i
  }
}
