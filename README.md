# D3 Tree

D3 dynamic tree project.

- TypeScript
- ESLint linting with Airbnb and Prettier styles
- Vitest unit tests
- Vite build system

## Installation

You should have Node.js 18.17.1 or higher, and pnpm installed.

Clone the repo.

```shell
git clone https://gitlab.com/narvin/d3tree
```

Install the dependencies.

```shell
cd d3tree
pnpm install
```

## Usage

Lint the code.

```shell
pnpm run lint
```

Run the unit tests.

```shell
pnpm test
```

Start a live development server.

```shell
pnpm run dev
```
